#include <boost/fusion/sequence.hpp>
#include <boost/fusion/include/sequence.hpp>
#include <QDebug>
#include <QString>
#include <QLatin1String>

int main()
{
  qDebug() << "Docker Images Test - Boost + Qt Core";

  /*
   * Check that a locale have been set
   * See: http://jaredmarkell.com/docker-and-locales
   */
  const QString a = QString::fromUtf8(u8"éöàäèü");
  const QString b = QString::fromUtf8(u8"éöàäèü");
  if( QString::compare(a, b) != 0 ){
    qDebug() << "Comparing a and b failed";
    qDebug() << " a: " << a << ", b: " << b;
  }

  /*
   * Check basically that we can use Boost
   */
  boost::fusion::vector<int, QString> fv(3, QLatin1String("three"));
  qDebug() << "fv(" << boost::fusion::at_c<0>(fv) << "," << boost::fusion::at_c<1>(fv) << ")";

  return 0;
}
