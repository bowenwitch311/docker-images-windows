# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

###############################
# Runners + global parts
###############################

# GitLab shared new Windows runners
# They currently not support Docker engine
#.windows_runner:
  #tags:
    #- shared-windows
    #- windows
    #- windows-1809

#.windows_runner:
  #tags:
    #- windows-aws

.windows_runner:
  tags:
    - windows-shell

.windows_runner_docker:
  tags:
    - docker-windows


# Some images are based on others.
# When creating them all, make shure we take the base currently building
# Adding stages is a simple solution (can it be done a other way ?)
stages:
  - build_stage1
  - build_stage2
  - build_stage3
  - build_stage4
  - build_stage5
  - test
  - deploy
  - test_deployed


variables:
  DOCKER_REGISTRY_BASE_PATH: registry.gitlab.com/scandyna/docker-images-windows


###############################
# Reusable templates
###############################

.build_rules:
  rules:
    - changes:
      - docker/**/*
      - tests/**/*
      - .gitlab-ci.yml
      when: on_success

# See: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
#  Typicall: build context
.build:
  rules:
    - !reference [.build_rules, rules]
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker version
    - docker system prune -f
    - cd docker
    - docker build --pull -f ${DOCKER_IMAGE_BASE_NAME}/Dockerfile -t ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building .
    - docker push ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  after_script:
    - docker logout $CI_REGISTRY
  extends:
    - .windows_runner


.test_tools_script:
  script:
    - cmake --version
    - powershell tests/scripts/windows_print_env.ps1
    - python --version
    - pip --version
    - conan --version
    - git --version
    - 7z
    - pwsh --version

.test_tools:
  stage: test
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends:
    - .test_tools_script
    - .windows_runner_docker

.test_tools_Gcc:
  stage: test
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends: .windows_runner_docker
  script:
    - !reference [.test_tools_script, script]
    - gcc --version

.test_tools_GccAndQt:
  stage: test
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends: .windows_runner_docker
  script:
    - C:\Qt\Tools\mingw730_64\bin\gcc.exe --version
    - C:\Qt\Tools\mingw730_32\bin\gcc.exe --version
    - powershell $env:QT_PREFIX_PATH/bin/qmake.exe -version


.test_script:
  before_script:
    - $env:PATH += ";$QT_PREFIX_PATH/bin"
  script:
    - mkdir build
    - cd build
    - cmake -G"$CMAKE_GENERATOR" $CMAKE_GENERATOR_EXTRA_ARGS $CMAKE_OPTIONS -DCMAKE_BUILD_TYPE=$BUILD_TYPE
            -DCMAKE_PREFIX_PATH="$BOOST_PREFIX_PATH;$QT_PREFIX_PATH" ../tests
    - cmake --build . --config $BUILD_TYPE
    - qtdiag --fonts
    - ctest --output-on-failure --verbose --build-config $BUILD_TYPE .

.test:
  stage: test
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends:
    - .test_script
    - .windows_runner_docker


.deploy:
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
    - docker tag ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:${CI_COMMIT_REF_NAME}
    - docker tag ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:${CI_COMMIT_REF_NAME} ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
    - docker push ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:${CI_COMMIT_REF_NAME}
    - docker push ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  after_script:
    - docker logout $CI_REGISTRY
  extends:
    - .windows_runner


.test_deployed_base:
  stage: test_deployed
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never


.test_tools_deployed:
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  extends:
    - .test_deployed_base
    - .test_tools_script
    - .windows_runner_docker

.test_tools_Gcc_deployed:
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  extends:
    - .windows_runner_docker
    - .test_deployed_base
  script:
    - !reference [.test_tools_script, script]
    - gcc --version

.test_deployed:
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  extends:
    - .test_deployed_base
    - .test_script
    - .windows_runner_docker

###############################
# Builds - servercore based
###############################

build_windows-cpp-base:
  stage: build_stage1
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-base
  extends: .build

build_windows-cpp-part-tools:
  stage: build_stage2
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-part-tools
  extends: .build
  needs: ["build_windows-cpp-base"]

build_windows-cpp-part-boost:
  stage: build_stage3
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-part-boost
  extends: .build
  needs: ["build_windows-cpp-part-tools"]

build_windows-cpp-part-mingw:
  stage: build_stage4
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-part-mingw
  extends: .build
  needs: ["build_windows-cpp-part-boost"]

build_windows-cpp-part-msvc:
  stage: build_stage4
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-part-msvc
  extends: .build
  needs: ["build_windows-cpp-part-boost"]

build_windows-cpp-win32_mingw73:
  stage: build_stage3
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win32_mingw73
  extends: .build
  needs: ["build_windows-cpp-part-tools"]

build_windows-cpp-win64_mingw73:
  stage: build_stage3
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw73
  extends: .build
  needs: ["build_windows-cpp-part-tools"]

build_windows-cpp-win64_mingw81:
  stage: build_stage3
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw81
  extends: .build
  needs: ["build_windows-cpp-part-tools"]

build_windows-cpp-msvc2019:
  stage: build_stage3
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-msvc2019
  extends: .build
  needs: ["build_windows-cpp-part-tools"]

build_windows-cpp-qt-5.14.2-win64_mingw73:
  stage: build_stage5
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_mingw73
  extends: .build
  needs: ["build_windows-cpp-part-mingw"]

build_windows-cpp-qt-5.14.2-win32_mingw73:
  stage: build_stage5
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win32_mingw73
  extends: .build
  needs: ["build_windows-cpp-part-mingw"]

build_windows-cpp-qt-5.14.2-win64_msvc2017_64:
  stage: build_stage5
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_msvc2017_64
  extends: .build
  needs: ["build_windows-cpp-part-msvc"]

###############################
# Builds - windows (full) based
###############################

#build_windows-cpp-part-tools-dotnet:
  #stage: build_stage3
  #variables:
    #DOCKER_IMAGE_BASE_NAME: windows-cpp-part-tools-dotnet
  #extends: .build
  #needs: ["build_windows-cpp-part-tools"]

build_windows-full-dotnet:
  stage: build_stage1
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-full-dotnet
  extends: .build

build_windows-full-dotnet-cpp-base:
  stage: build_stage2
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-full-dotnet-cpp-base
  extends: .build
  needs: ["build_windows-full-dotnet"]

build_windows-full-dotnet-cpp-part-tools:
  stage: build_stage3
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-full-dotnet-cpp-part-tools
  extends: .build
  needs: ["build_windows-full-dotnet-cpp-base"]

build_windows-build-qt-win64_mingw81:
  stage: build_stage4
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-build-qt-win64_mingw81
  extends: .build
  needs: ["build_windows-full-dotnet-cpp-part-tools"]

###############################
# Test tools
###############################

test_windows-cpp-win32_mingw73_tools:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win32_mingw73
  extends: .test_tools_Gcc
  needs: ["build_windows-cpp-win32_mingw73"]

test_windows-cpp-win64_mingw73_tools:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw73
  extends: .test_tools_Gcc
  needs: ["build_windows-cpp-win64_mingw73"]

test_windows-cpp-win64_mingw81_tools:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw81
  extends: .test_tools_Gcc
  needs: ["build_windows-cpp-win64_mingw81"]

test_windows-build-qt-win64_mingw81_tools:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-build-qt-win64_mingw81
  extends: .test_tools_Gcc
  needs: ["build_windows-build-qt-win64_mingw81"]

test_windows-cpp-msvc2019_tools:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-msvc2019
  extends: .test_tools
  needs: ["build_windows-cpp-msvc2019"]

test_windows-cpp-qt-5.14.2-win64_mingw73_tools:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_mingw73
  extends: .test_tools
  needs: ["build_windows-cpp-qt-5.14.2-win64_mingw73"]

test_windows-cpp-qt-5.14.2-win64_mingw73_tools_gcc:
  variables:
    QT_PREFIX_PATH: "C:/Qt/5.14.2/mingw73_64"
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_mingw73
  extends: .test_tools_GccAndQt
  needs: ["build_windows-cpp-qt-5.14.2-win64_mingw73"]

###############################
# Test build
###############################

test_windows-cpp-qt-5.14.2-win64_mingw73:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_mingw73
    CMAKE_GENERATOR: "MinGW Makefiles"
    CMAKE_OPTIONS: -DTEST_QT_WIDGETS=ON
    BOOST_PREFIX_PATH: "C:/Libraries/boost_1_73_0"
    QT_PREFIX_PATH: "C:/Qt/5.14.2/mingw73_64"
    BUILD_TYPE: Debug
  extends: .test
  needs: ["build_windows-cpp-qt-5.14.2-win64_mingw73"]

test_windows-cpp-qt-5.14.2-win32_mingw73:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win32_mingw73
    CMAKE_GENERATOR: "MinGW Makefiles"
    CMAKE_OPTIONS: -DTEST_QT_WIDGETS=ON
    BOOST_PREFIX_PATH: "C:/Libraries/boost_1_73_0"
    QT_PREFIX_PATH: "C:/Qt/5.14.2/mingw73_32"
    BUILD_TYPE: Debug
  extends: .test
  needs: ["build_windows-cpp-qt-5.14.2-win32_mingw73"]

test_windows-cpp-qt-5.14.2-win64_msvc2017_default_debug:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_msvc2017_64
    CMAKE_GENERATOR: "Visual Studio 16 2019"
    CMAKE_OPTIONS: -DTEST_QT_WIDGETS=ON
    BOOST_PREFIX_PATH: "C:/Libraries/boost_1_73_0"
    QT_PREFIX_PATH: "C:/Qt/5.14.2/msvc2017_64"
    BUILD_TYPE: Debug
  extends: .test
  needs: ["build_windows-cpp-qt-5.14.2-win64_msvc2017_64"]

###############################
# Deploy
###############################

deploy_windows-cpp-win64_mingw73:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw73
  extends: .deploy

deploy_windows-cpp-win32_mingw73:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win32_mingw73
  extends: .deploy

deploy_windows-cpp-win64_mingw81:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw81
  extends: .deploy

deploy_windows-build-qt-win64_mingw81:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-build-qt-win64_mingw81
  extends: .deploy

deploy_windows-cpp-msvc2019:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-msvc2019
  extends: .deploy

deploy_windows-cpp-qt-5.14.2-win64_mingw73:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_mingw73
  extends: .deploy

deploy_windows-cpp-qt-5.14.2-win32_mingw73:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win32_mingw73
  extends: .deploy

deploy_windows-cpp-qt-5.14.2-win64_msvc2017_64:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_msvc2017_64
  extends: .deploy

###############################
# Tests deployed tools
###############################

test_windows-cpp-win32_mingw73_tools_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win32_mingw73
  extends: .test_tools_deployed
  needs: ["deploy_windows-cpp-win32_mingw73"]

test_windows-cpp-win64_mingw73_tools_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw73
  extends: .test_tools_deployed
  needs: ["deploy_windows-cpp-win64_mingw73"]

test_windows-cpp-win64_mingw81_tools_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-win64_mingw81
  extends: .test_tools_Gcc_deployed
  needs: ["deploy_windows-cpp-win64_mingw81"]

test_windows-build-qt-win64_mingw81_tools_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-build-qt-win64_mingw81
  extends: .test_tools_Gcc_deployed
  needs: ["deploy_windows-build-qt-win64_mingw81"]

test_windows-cpp-msvc2019_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-msvc2019
  extends: .test_tools_deployed
  needs: ["deploy_windows-cpp-msvc2019"]

test_windows-cpp-qt-5.14.2-win64_mingw73_tools_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_mingw73
    #QT_PREFIX_PATH: "C:/Qt/5.14.2/mingw73_64"
  extends: .test_tools_deployed
  needs: ["deploy_windows-cpp-qt-5.14.2-win64_mingw73"]

test_windows-cpp-qt-5.14.2-win64_msvc2017_tools_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_msvc2017_64
    #QT_PREFIX_PATH: "C:/Qt/5.14.2/msvc2017_64"
  extends: .test_tools_deployed
  needs: ["deploy_windows-cpp-qt-5.14.2-win64_msvc2017_64"]

###############################
# Tests deployed build
###############################

test_windows-cpp-qt-5.14.2-win64_mingw73_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_mingw73
    CMAKE_GENERATOR: "MinGW Makefiles"
    CMAKE_OPTIONS: -DTEST_QT_WIDGETS=ON
    BOOST_PREFIX_PATH: "C:/Libraries/boost_1_73_0"
    QT_PREFIX_PATH: "C:/Qt/5.14.2/mingw73_64"
    BUILD_TYPE: Debug
  extends: .test_deployed
  needs: ["deploy_windows-cpp-qt-5.14.2-win64_mingw73"]

test_windows-cpp-qt-5.14.2-win32_mingw73_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win32_mingw73
    CMAKE_GENERATOR: "MinGW Makefiles"
    CMAKE_OPTIONS: -DTEST_QT_WIDGETS=ON
    BOOST_PREFIX_PATH: "C:/Libraries/boost_1_73_0"
    QT_PREFIX_PATH: "C:/Qt/5.14.2/mingw73_32"
    BUILD_TYPE: Debug
  extends: .test_deployed
  needs: ["deploy_windows-cpp-qt-5.14.2-win32_mingw73"]

test_windows-cpp-qt-5.14.2-win64_msvc2017_64_deployed:
  variables:
    DOCKER_IMAGE_BASE_NAME: windows-cpp-qt-5.14.2-win64_msvc2017_64
    CMAKE_GENERATOR: "Visual Studio 16 2019"
    CMAKE_OPTIONS: -DTEST_QT_WIDGETS=ON
    BOOST_PREFIX_PATH: "C:/Libraries/boost_1_73_0"
    QT_PREFIX_PATH: "C:/Qt/5.14.2/msvc2017_64"
    BUILD_TYPE: Debug
  extends: .test_deployed
  needs: ["deploy_windows-cpp-qt-5.14.2-win64_msvc2017_64"]
