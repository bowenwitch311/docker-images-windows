$ErrorActionPreference = 'Stop'

Write-Output "Getting helper scripts from scandyna/powershell-scripts repo ..."

# Link for v 0.0.4
Invoke-WebRequest -Uri "https://gitlab.com/scandyna/powershell-scripts/-/package_files/54876416/download" -OutFile "C:\TEMP\powershell_scripts.zip" -ErrorAction Stop
Expand-Archive -Path "C:\TEMP\powershell_scripts.zip" -DestinationPath "C:\TEMP\" -ErrorAction Stop

ls C:\TEMP\

Write-Output "Getting helper scripts from scandyna/powershell-scripts repo done"
